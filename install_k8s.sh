#!/bin/bash

sudo apt update -y
sudo apt upgrade -y

# Disable Swap
echo "---------------- Disable Swapp --------------"
sleep 3
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Add Kernel Parameters
echo "---------------- Load the required kernel modules -----------------------"
sleep 3
sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF
sudo modprobe overlay
sudo modprobe br_netfilter


echo "----------------  Configure the critical kernel parameters for Kubernetes  ------------------"
sleep 3
sudo tee /etc/sysctl.d/kubernetes.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

echo "---------------------  Reload the changes  --------------------------------"
sleep 3
sudo sysctl --system


# Install Containerd Runtime
echo "-------------------------  Install containerd and its dependencies  -----------------------------------"
sleep 3
sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates

echo "-------------------------  Enable the Docker repository  ------------------------------------"
sleep 3
sudo curl -kfsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

echo "-------------------------  Update the package list and install containerd  ------------------------"
sleep 5
sudo apt update -y
sudo apt install -y containerd.io

echo "------------------------  Configure containerd to start using systemd as cgroup  ----------------------"
sleep 3
containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml

echo "------------------------  Restart and enable the containerd service  -------------------------------"
sleep 3
sudo systemctl restart containerd
sudo systemctl enable containerd


# Add Apt Repository for Kubernetes
echo "--------------------------  Add the Kubernetes repositories  -----------------------------------"
sleep 3
# curl -s -k https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/kubernetes-xenial.gpg -> Not working for ubuntu22.jammy release
# see https://forum.linuxfoundation.org/discussion/864693/the-repository-http-apt-kubernetes-io-kubernetes-xenial-release-does-not-have-a-release-file

#add this(no newline):
#deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /
#to the file: /etc/apt/sources.list.d/kubernetes.list

#get the key:
#curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

#sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"



# Define the line to be added
line="deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /"

# Define the file path
file_path="/etc/apt/sources.list.d/kubernetes.list"

# Check if the file exists, if not, create it
if [ ! -f "$file_path" ]; then
    touch "$file_path"
fi

# Check if the line already exists in the file, if not, add it
if ! grep -qF "$line" "$file_path"; then
    echo "$line" | sudo tee -a "$file_path" > /dev/null
    echo "Line added to $file_path"
else
    echo "Line already exists in $file_path"
fi


# Install Kubectl, Kubeadm, and Kubelet
echo "-----------------------------  Install Kubectl, Kubeadm, and Kubelet  -----------------------"
sleep 3
sudo apt update
sudo apt install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
